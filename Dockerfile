FROM scratch

LABEL maintaner="Albert Achterkamp <bert.achterkamp@protonmail.com>"

COPY . .

EXPOSE 8080

CMD ["./main"]
